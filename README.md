This repo is the website of the PFAS Data Hub Project, published on [pdh.cnrs.fr](pdh.cnrs.fr)

It uses the data that is produced by the Data project: [https://gitlab.com/pfas-data-hub/pdh-data](https://gitlab.com/pfas-data-hub/pdh-data)

# Install 
clone the project
create a `.env` file with the path to the final folder of data project. 

# Desciption
This project contains different services, orchestrated in a docker compose file: 
* Jekyll (`frontend`) that generates the static website in the folder `frontend/_site`.
* a REST API (`data_api`), done in python with FastAPI, who allows to query the PFAS data. You can see the doc at `/api/docs`
* a redis (`redis`) to have a cache for the API. 
* a NGINX server (`nginx`), serving the static content at `/` and the data API at `/api`

The jekyll website has one page that is more complex: the map, which uses the REST PI. 


# Usage

## Adding a page or a blog post
Coming soon... 

# Librairies used
This website is meant to be a simple as possible. This means using a few libraries as possible to ensure a low maintenance, 
even in a few years time. Most of the pages are simple Markdown rendered by Jekyll with the default theme (minima). 

The map page is the one that is complex. It is done almost exclusively in pure HTML / JS.
The map itself uses Leaflet, but that will likely change. 

Librairies in use on the website:
* [Jekyll](https://jekyllrb.com)
* Its default theme [Minima](https://github.com/jekyll/minima)
* [Polyglot](https://github.com/untra/polyglot), a Jekyll plugin to manage the internationalisation of the website. 
* [MultiSelect](https://codeshack.io/multi-select-dropdown-html-javascript/): a minimalist way to have a nice multiselect by David Adams. 
* [Bootstrap 5](https://getbootstrap.com/): a well-known css framework to make the site beautiful. 

We take the occasion to warmly thank the contributors to these projects! 
