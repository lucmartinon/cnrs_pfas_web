---
layout: page
title: Methodology
permalink: /methodology/
lang: en
last_update: 2024-11-06
---

- toc
{:toc}
# Introduction

## The Forever Pollution Project
The [Forever Pollution project](https://foreverpollution.eu/) (later shortened as FPP), is a cross border investigation 
that was published in February 2023 by 16 European medias.
The FPP lead an unprecedented effort to document contaminations by per- and polyfluoroalkyl substances (PFAS) , listing data sources and establishing [a map of known 
and suspected PFAS contaminations in Europe](https://www.lemonde.fr/en/les-decodeurs/article/2023/02/23/forever-pollution-explore-the-map-of-europe-s-pfas-contamination_6016905_8.html). 
Over 100 different data sources were collected and merged to establish the map. 

The methodology of the FPP project was based the one of the "[PFAS Sites and Community Resources Map](https://experience.arcgis.com/experience/12412ab41b3141598e0bb48523a7c940/)" 
of the [PFAS Project Lab](https://pfasproject.com/) (Boston, USA), with some important changes. The methodology was [published by Le Monde](https://assets-decodeurs.lemonde.fr/decodeurs/medias/foreverpollution/Methodology___The_Map_of_Forever_Pollution_2023.02.23.pdf)
together with the articles and the map. 
A few months after the journalistic publications, a scientific article was published in _Environmental Science & Technology_[^Cordner-A-et-al], with more context and discussion about the project outcomes.

## The PDH project
At the end of 2023, the CNRS expressed interest in continuing the work of the FPP project, with the objectives of making 
the data more complete and robust, as well as easier to reuse.
For this, it was necessary to come back at the raw data of each data source used by the FPP project and to reprocess this data with two main changes: 
* Keep all data available (the FPP project kept only values above a certain threshold, and only 6 PFAS individual concentrations)
* Do all the data work using python & sql scripts. The idea is that mistakes are inevitable on such a project, 
but by avoiding any work "by hand", we keep a trace of the choices we made, and we can easily correct them. 

The aim of this methodology is to focus on the concepts, while the processsing of the data is explained in the 
page [Data Processing](/data_doc), and the final data format in [Data Format](/data_format) and

# 1. Geographic scope of the research 

PFAS contamination sites were researched in the 27 Member States of the European Union (EU), in the United Kingdom (UK), 
and countries of the European Economic Area (EEA) and the European Free Trade Association (EFTA): Norway, Iceland, Liechtenstein, Switzerland.

# 2. Datasets and data sources

The PDH data combines data coming from external sources, as well as data that was compiled by the FPP project. 
We call dataset a unique origin of data, each identified by a dataset ID and a dataset name. Most datasets come from a unique source, 
but this is not always the case, especially the datasets that were compiled by the FFP project by OSINT research may have different sources.

The raw data of each dataset is published on the [GitLab repository of the project](https://gitlab.com/pfas-data-hub/pdh-data){:target="_blank"}, 
as well all the processing of the data that is done on it, so that it is possible to investigate on suspicion of mistakes in the final data.

The methodological notes about each dataset and its precessing are writen directly in the code that processes them. 
These comments are also available [on the datasets page](/datasets), as well as links to the raw data and to the code. 

In some cases, the data source is not a file, but a website that is scrapped or a data API that is used. In such cases, 
the data is updated each 30 days, allowing for the map to be kept up to date. Such datasets are marked as "dynamic" [on the datasets page](/datasets).

# 3. PFAS contamination sites
The Map of Forever Pollution includes four categories of sites: 
* Known contamination sites
* PFAS production facilities
* Known PFAS users
* Presumptive contamination sites
  
This categorisation is close the one developed by the FPP, which itself was largely based on the peer-reviewed methodology[^Salvatore-D-et-al] 
of the [PFAS project Lab map](https://pfasproject.com/). We introduced a new category for the PFAS production facilities, 
which were previously in the "Known" category. The idea behind was that PFAS contamination is certain around production sites. 
While this remains entirely true, we chose to separate these categories because it makes them clearer from the data point 
of view: Known points are now only points where we have concentration data. 

## 3.1 Known contamination sites  
Known PFAS contamination sites are sites where PFAS have been detected with air, water or solids testing. 
These data points have a lot of extra fields that indicate the details of the concentrations found (matrix, date,
concentration for each substance, etc.)

## 3.2 PFAS production facilities 
Current and legacy PFAS producers are industrial facilities currently manufacturing PFAS, and industrial facilities 
which have manufactured PFAS such as PFOS, PFOA or Teflon-like products in the past.
PFAS producers are companies which: 
- synthesise PFAS to sell them as 'ingredients' to PFAS users (= strictly producers)
- synthesise PFAS and use them to manufacture their own fluoropolymers (which include: PTFE, PVDF, side-chain fluorinated polymers, perfluoropolyethers, fluoroelastomers) (= both producers and users)

The list of such facilities was established by the FPP, it is the dataset `13`.
 
## 3.3  Known PFAS users
The category "Known PFAS Users" was introduced in the FPP, it was not defined in the Salvatore et al. (2022) peer-reviewed methodology[^Salvatore-D-et-al].

"Known PFAS Users" are locations for which there is evidence of PFAS use, but no testing data, and which can be considered likely to be contamination sources. 

For example, companies which buy fluoropolymers such as PTFE, ECTFE or FEP in the form of pellets to manufacture their 
own branded PTFE-containing products or thermoplastics items can be considered neither as PFAS producers nor as 
presumptive contamination sites. They were therefore categorised as "Known PFAS Users". 

Similarly, AFFF manufacturers use PFAS to manufacture firefighting foams, but they are not included in the list of 
presumptive contamination sites in correlation to industrial activity. 
They were also categorised as "Known PFAS Users". 

## 3.4 Presumptive contamination sites
Presumed sites of PFAS contamination are sites where testing has not confirmed the presence of PFAS, but which can be 
presumed to be contaminated on the basis of scientific investigations and expert advice[^Salvatore-D-et-al].

This approach posits that, "in the absence of high-quality data to the contrary, PFAS contamination is probable near 
facilities known to produce, use, and/or release PFAS, and to protect public health, the existence of PFAS in these 
locations should be presumed until high-quality testing data is available".

Several States in the U.S.[^Salvatore-D-et-al], the EU Commission, and the French environmental authorities[^DREAL] have 
used a similar approach to identify sampling targets for PFAS contamination based on facility type. 
In October 2022, the EU Commission listed nine industrial activities "where PFAS are likely used 
(textiles, leather, carpets, paper, paints and varnishes, cleaning products, metal treatments, car washes, plastic/resins/rubber)"[^EU_pfas_activities]
based on codes from the European Nomenclature of Economic Activities (NACE). 
In France, the BRGM (Bureau de Recherches Géologiques et Minières) has listed 117 NAF codes corresponding to industrial activities correlated to PFAS use[^BRGM_InfoTerre].

Based on the methodology developed by Salvatore et al. (2022)[^Salvatore-D-et-al], PFAS contamination can be presumed around three types of facilities: 
- fluorinated aqueous film-forming foam (AFFF) discharge and storage sites
- certain industrial facilities
- sites related to PFAS-containing waste

A detailed methodology for locating EU sites based on these three categories is below.

### 3.4.1 Fluorinated aqueous film-forming foam (AFFF) discharge sites
Fluorinated aqueous film-forming foam (AFFF) has been used extensively for fire training and extinguishing fuel-based fires. 

Therefore, PFAS contamination is expected wherever AFFF has been discharged and stored, including[^Salvatore-D-et-al]:
- military sites (military bases, military air bases and airports, military training camps, NATO bases, and formerly used defence sites) 
- commercial civilian airports
- firefighting training sites (including fire stations)
- fire suppression locations (aeroplane and railroad crash sites, oil and gas extraction sites, petroleum refineries, bulk storage facilities, chemical manufacturing plants)

Within the EU, AFFF containing PFOS were banned in 2006 with a complete phase-out in 2011[^SCA_AFFF_survey], but replacement AFFFs still contained PFAS. 
Starting in 2020, the use of AFFF containing more than 25 ppb of PFOA or its salts as well as those containing more than 
1000 ppb of one or a combination of PFOA related substances were also restricted[^EU_Com_Reg]. 
A proposal for a EU-wide restriction of PFAS in firefighting foams was submitted by the European Chemicals Agency (ECHA) in February 2022[^ECHA_AFFF_ban].

### 3.4.2 Industrial facilities
Salvatore et al. (2022) used the North American Industry Classification System (NAICS)[^NAICS] in order to identify 
industrial activities that are presumed PFAS users and contamination sources and establish a "suspect" list[^PLP_NAICS_list] 
of 38 NAICS codes that are likely sources of PFAS contamination.

[TODO]: link from this list to the EPRTR filter?

### 3.4.3 Sites related to PFAS-containing waste
PFAS can be present in wastewater and solid waste, resulting in contaminated effluent and sludge from wastewater 
treatment plants (WWTPs), and landfill leachate or incinerator ash[^Salvatore-D-et-al].

The following sites are included in the Presumptive contamination sites: 
- WWTPs
- landfills for non-hazardous and hazardous waste
- incinerators

## 3.5 Types of sites as shown on the map 
[TODO]: not satisfying, should be a sub_category really. 
We sorted the types of sites under six categories:
 
- Industrial site 
- Waste management site 
- Airport
- Military site 
- Sampling location (all "Known" points are marked as Sampling location, even though they may be on an Airport or an Industrial site)
- Firefighting incident / training 
- Other


# 4. PFAS identification

## 4.1 PFAS Reference list
The OECD list of 4,730 PFAS[^OECD-PFAS-List] was used as reference list of existing PFAS. The csv file [substances.csv](https://gitlab.com/pfas-data-hub/pdh-data/-/blob/main/data/settings/substances.csv) 
is directly based on the OECD Excel file. Columns after R (Notes) were deleted, and a new column was added, Acronym, which 
replaces the (often too long) substance name when available. 

Contrarily to the FPP, the PDH project keeps all substances values rather than focusing on a shortlist of interesting PFAS. 
This is one of the main changes in the methodology, making the data much more complete. 

## 4.2 PFAS synonyms
In order to identify PFAS in different datasets, a [list of PFAS synonym](https://gitlab.com/pfas-data-hub/pdh-data/-/blob/main/data/settings/substance_synonyms.csv) 
was established, first based on the synonyms columns of the OECD list, then extended manually each time
a dataset presented an unknown name for a PFAS substance. Research to identify the substance were done in the grey 
literature and using [PubChem](https://pubchem.ncbi.nlm.nih.gov/).

Should you spot a mistake in this list of synonyms, please inform us! We will correct the list and reprocess our data 
based on the corrected list.

## 4.3 Isomers
In some datasets, the distinction is made between branched and linear isomers of the same PFAS. We kept these values 
separated in the data processed, and always added a "total" substance in case it was not present in the source data.

# 5. Contamination values

## 5.1 Units 
The values in the source datasets were presented under various units, which depend a lot on the matrix where the sampling was done. 
We converted the values to have the values expressed as what is normally considered as **part per trillion**. In details:
- concentrations in **liquids volume** were converted in **nanograms per liter**. 
- concentrations in **gaz volume** were converted in **picogram per cubic meter**. 
- concentrations in wet **weight** were converted in **nanograms per kilos (wet weight)**. 
- concentrations in dry **weight** were converted in **nanograms per kilos (dry weight)**.

## 5.2 Values below the limit of quantification
When a sampling is done and a PFAS is not detected, we only know that the concentration of this PFAS is below the 
"limit of detection" (LOD) and/or "limit of quantification" (LOQ) of the test. Such values are indicated in many datasets. 
We kept these values in the data processing, in a separate field `less_than`, so that the `value` column can be summed 
directly without leading to meaningless result (see [data format](/data_format)).

Very few datasets mentioned both the LOD and LOQ. In such cases we considered:
- values <LOD are necessarily also <LOQ, but where marked as "less than [LOD]"
- values >LOD but <LOQ were marked as "less than [LOQ]"

Some dataset mentioned values <LOQ or <LOD without mentioning was the actual value of the LOD/LOQ was. These values were ignored. 

## 5.5 Multiple sampling on one site 
We tried to keep as many values as possible, but we kept only one value per dataset, coordinates, date, substance, matrix.
In case more than one value for the same combinaison of these fields was present in a dataset, we kept only the highest value. 
This was implemented to prevent choosing randomly the value in such a case, but I am not sure this case actually happened in reality. 

(#TODO: do you think it's necessary to estimate this?)

Almost everytime, we simply kept all the values of the original data. 

---
[^Cordner-A-et-al]: Cordner, A et al. (2024) _PFAS Contamination in Europe: Generating Knowledge and Mapping Known and Likely Contamination with "Expert-Reviewed" Journalism_  
    Environmental Science & Technology 58(15)  
    [10.1021/acs.est.3c09746](https://doi.org/10.1021/acs.est.3c09746)  

[^Salvatore-D-et-al]: Salvatore, D. et al. (2022). _Presumptive Contamination: A New Approach to PFAS Contamination Based on Likely Sources_.  
    Environmental Science & Technology Letters, 9(11), 983-990      
    [10.1021/acs.estlett.2c00502](https://doi.org/10.1021/acs.estlett.2c00502) 

[^Wang-Z-et-al]: Wang Z. et al. _Global emission inventories for C4–C14 perfluoroalkyl carboxylic acid (PFCA) homologues from 1951 to 2030_  
    Part I: _Production and emissions from quantifiable sources_.  
    Environment international 70 (2014): 62-75, [10.1016/j.envint.2014.04.013](https://doi.org/10.1016/j.envint.2014.04.013).   
    Part II: _The remaining pieces of the puzzle_.  
    Environment international 69 (2014): 166-176, [10.1016/j.envint.2014.04.006](https://doi.org/10.1016/j.envint.2014.04.006)

[^OECD-PFAS-List]: OECD: Summary report on the new comprehensive global database of Per- and Polyfluoroalkyl Substances (PFASs)  
    [10.1787/1a14ad6c-en](https://doi.org/10.1787/1a14ad6c-en)  
    [Excel with the PFAS list](https://www.oecd.org/content/dam/oecd/en/topics/policy-sub-issues/risk-management-risk-reduction-and-sustainable-chemistry2/pfas-report-support-materials/global%20database%20of%20per%20and%20polyfluoroalkyl%20substances.xlsx) 

[^DREAL]: DREAL Auvergne-Rhône-Alpes, 2 September 2024
    [PFAS: la surveillance des rejets industriels dans l'eau et les milieux aquatiques](https://www.auvergne-rhone-alpes.developpement-durable.gouv.fr/pfas-la-surveillance-des-rejets-industriels-dans-l-a23561.html)

[^EU_pfas_activities]: Commission Staff Working Document. Impact Assessment Report accompanying the document Proposal for a Directive of the European Parliament and of the Council amending Directive 2000/60/EC establishing a framework for Community action in the field of water policy, Directive 2006/118/EC on the protection of groundwater against pollution and deterioration and Directive 2008/105/EC on environmental quality standards in the field of water policy  
    {COM(2022) 540 final} - {SEC(2022) 540 final} - {SWD(2022) 543 final}, 26 October 2022.  
    <https://environment.ec.europa.eu/system/files/2022-10/Staff%20Working%20Document%20-%20Impact%20Assessment%20Report%20accompanying%20the%20Proposal_0.pdf>{:target="_blank"}

[^BRGM_InfoTerre]:  InfoTerre, BRGM - Base de données des corrélations Activités-Polluants, BD ActiviPoll  
    <https://ssp-infoterre.brgm.fr/fr/bd-activipoll/recherche#tab-2>{:target="_blank"}

[TODO]: this one below seems weird to me, we  could reference directly the regulation rather than this report quoting it. 
[^SCA_AFFF_survey]: Swedish Chemicals Agency (2015). Survey of Fire Fighting Foam.  
    <https://www.kemi.se/en/publications/pms/2015/pm-5-15-survey-of-fire-fighting-foam>{:target="_blank"}

[^EU_Com_Reg]: Commission Regulation (EU) 2017/1000 of 13 June 2017 amending Annex XVII to Regulation (EC) No 1907/2006 of the European Parliament and of the Council concerning the Registration, Evaluation, Authorisation and Restriction of Chemicals (REACH) as regards perfluorooctanoic acid (PFOA), its salts and PFOA-related substances.

[^ECHA_AFFF_ban]: European Chemicals Agency, _Proposal to ban "forever chemicals" in firefighting foams throughout the EU, 22 February 2022_  
    <https://echa.europa.eu/fr/-/proposal-to-ban-forever-chemicals-in-firefighting-foams-throughout-the-eu>{:target="_blank"}

[^NAICS]: North American Industry Classification System, <https://www.census.gov/naics/>{:target="_blank"}

[^PLP_NAICS_list]:  PFAS Lab Project NAICS Codes
    <https://docs.google.com/spreadsheets/d/10GbwbsH4W_UFVgno-fqTdE5NHKRrLqPIgK_6_sD_Tbc/edit#gid=2112574405>{:target="_blank"}

