---
layout: page
lang: en
permalink: /participate/
title: Participate
last_update: 2024-11-06
---
The PDH project open to contributions.

### If you find a mistake in our data
Please reach out to us so we can correct it. Either by email or you can open a gitlab issue in the data project if you're familiar with gitlab (link at the bottom of the page)

### If you know of data that is not yet here
Please let us know about datasets that are not yet in PDH. We are particularly looking for datasets that have recurring data, such as an api with the tests of drinking waters, or any kind of data that we can automate the actualisation.

For a Samplings dataset to be usable in PDH, it needs to have: 
* the date of the sampling
* the location, preferably as coordinates, but we also have some data with only the city (however they are not shown on the map)
* individual values of PFAS concentrations

