---
layout: page
title: Data format
permalink: /data_format/
lang: en
last_update: 2024-11-06
---

<table>
    <tr>
        <th>Field name</th>
        <th>Description</th>
        <th>Example</th>
    </tr>
  {% for row in site.data.data-format_-_main %}
    <tr>
        <td><b>{{row.field_name}}</b><BR/><code>{{row.type}}</code></td>
        <td>
            {{row.description}}
            {% if row.mandatory == "TRUE"  %}<BR/><span class="badge text-black-50 bg-warning-subtle">Mandatory</span> {% endif %}
            {% if row.only_sampling == "TRUE" %}<BR/><span class="badge text-black-50 bg-info-subtle">Only for known points</span> {% endif %}
            {% if row.value_list == "TRUE" %}<BR/><span class="badge text-black-50 bg-success-subtle">List of values</span> {% endif %}
</td> 
        <td>{{row.example}}</td>
    </tr>
  {% endfor %}
</table>

## Format of `pfas_values`
The field `pfas_values` is a JSON array, allowing to have as many individual PFAS concentrations as needed. 
The json objects of the array have the following fields. These fields are never all present: `value` and `less than` are never together, and `isomer` is present only if needed.  


<table>
  {% for row in site.data.data-format_-_pfas_value %}
    {% if forloop.first %}
    <tr>
      {% for pair in row %}
        <th>{{ pair[0] }}</th>
      {% endfor %}
    </tr>
    {% endif %}

    {% tablerow pair in row %}
      {{ pair[1] }}
    {% endtablerow %}
  {% endfor %}
</table>

An example of `pfas_values` could be the following: 

```json
[
  {
    "cas_id": "1763-23-1",
    "unit": "ng/l",
    "substance": "PFOS",
    "value": "539.0"
  },
  {
    "cas_id": "1763-23-1",
    "unit": "ng/l",
    "substance": "PFOS",
    "isomer": "branched",
    "value": "37.0"
  },
  {
    "cas_id": "1763-23-1",
    "unit": "ng/l",
    "substance": "PFOS",
    "isomer": "linear",
    "value": "502.0"
  },
  {
    "cas_id": "335-67-1",
    "unit": "ng/l",
    "substance": "PFOA",
    "less_than": "0.3"
  },
  {
    "cas_id": "307-24-4",
    "unit": "ng/l",
    "substance": "PFHxA",
    "value": "1340.0"
  }
]
```