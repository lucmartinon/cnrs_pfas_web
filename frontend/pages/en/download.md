---
layout: page
lang: en
permalink: /download/
title: Downloads
last_update: 2024-11-06
---

The complete dataset is available:
* <a download="pdh_data.csv" {% static_href %} href="/download/full.csv"{% endstatic_href %}>as a csv file</a>, 
* <a download="pdh_data.parquet" {% static_href %} href="/download/full.parquet"{% endstatic_href %}>as a parquet file</a>. 

The format of these exports is explained [here](/data_format)


## Exports
You can also export the data from the map, which gives you much more filters options (per category, dataset, substance, etc). 
However, this excludes the points for which we have only the city and not the precise coordinates, as they are not shown on the map. 

The map export also allows to have a single column per substance. This is useful if you don't want to have to deal with the `pfas_values` in a json array field. 
Be aware that this means that there will be one column per substance, so in some cases, dozens or even hundreds of columns. 

You can also export the data by dataset from the [datasets](/datasets) page. 


## Data api
A minimalistic API was developed for the map to work. If it is more convenient, feel free to use it to export our data. 
Be careful that the API can change without notice, if you plan on using it regularly please let us know. 
It has an automated doc page that allows to try the API directly: 
<a {% static_href %} href="/api/docs" {% endstatic_href %} target="_blank">https://pdh.cnrs.fr/api/docs</a>


## Example notebook for data analysis
Please find [here](https://colab.research.google.com/drive/1D_U_HX_D3a9KNk-Y79lF0iehRynE6Rt9){:target="_blank"}
a Google Collab notebook with different examples of data filtering and data transformations. 
You can of course download it to run it somewhere else than Google Collab if you prefer. 


<div class="alert alert-primary" role="alert">
If you have any question on the data, don't hesitate to contact us (email below). We are happy to help! 
</div>
