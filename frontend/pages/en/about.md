---
layout: page
lang: en
permalink: /about/
title: About
last_update: 2025-02-19
---

## Project history
Early 2023, the [Forever Pollution Project](https://foreverpollution.eu/){:target="_blank"}   showed that nearly 23,000 sites all over Europe are contaminated 
by the “forever chemicals” PFAS. This unique collaborative cross-border and cross-field investigation 
by 16 European newsrooms revealed an additional 21,500 presumptive contamination sites due to current or 
past industrial activity. PFAS contamination spreads all over Europe, as shown on 
[the map](https://www.lemonde.fr/en/les-decodeurs/article/2023/02/23/forever-pollution-explore-the-map-of-europe-s-pfas-contamination_6016905_8.html){:target="_blank"} 
created by the project. 


To establish this map, each newsroom in the project researched PFAS contamination data in their own area, resulting in over 100 data sources,
that had to be standardised to be displayed on the map. The data was published at the same time than the map, as well as the methodology 
developed for the project (first [published](https://assets-decodeurs.lemonde.fr/decodeurs/medias/foreverpollution/Methodology___The_Map_of_Forever_Pollution_2023.02.23.pdf){:target="_blank"} 
by _Le Monde_, then as a [peer-reviewed article](https://pubs.acs.org/doi/10.1021/acs.est.3c09746){:target="_blank"} in _Environmental Science & Technology_)  .

![Screenshot of the map of the Forever Pollution Project](../assets/images/The-Map-of-forever-pollution-Forever-Pollution-project-%C2%A9Le-Monde-crop.png "Screenshot of the map of the Forever Pollution Project")

After publication, different research projects contacted us to re-use the data, and in January 2024 the PDH project was 
initiated by the CNRS. Its goals: 
* To have a technical and operational solution so that the data keeps on being updated
* To make the data better:
  * More complete (keep the values below detection limits, keep all individual PFAS substances concentrations)
  * More robust from a methodology point of view (eliminating all manual operations)
* To have a website where the data would be accessible, both as a geo-visualisation, and as exports
* To be as collaborative as possible: that our data is the source of other projects, but also that other researchers indicate us available data that is not in our dataset yet.


## Privacy policy
### The PDH project itself
The PFAS Data Hub project collects and processes data that does not contain personal data. As such, there is no need for a privacy policy.

### This website
This website uses [Matomo](https://matomo.org/){:target="_blank"} for analytics, with the most privacy-oriented options possible. 
Matomo is listed by the French data authority CNIL as a service provider that can be used without asking for user 
consent (see here), when the [following options](https://www.cnil.fr/sites/cnil/files/atoms/files/matomo_analytics_-_exemption_-_guide_de_configuration.pdf){:target="_blank"} are used. 


This means in particular: 
* The data is collected with the sole purpose of analytics
* The IPs are anonymised before being collected
* We respect the DoNotTrack preference (see [here](https://en.wikipedia.org/wiki/Do_Not_Track){:target="_blank"} for more info)

This website does not collect any user data besides Matomo. 

## Hosting
This website is hosted by the CNRS via [Huma-Num](https://www.huma-num.fr/){:target="_blank"}, a research infrastructure dedicated to
providing IT solutions for research projects. Huma-Num also hosts the Matomo instance that we use, so that the data
always remain in France, on servers administrated by Huma-Num.

We warmly thank Huma-Num for their support! 
![Huma-Num's logo](https://www.huma-num.fr/wp-content/uploads/2024/11/HN_LOGO_COULEUR-CMJN-png.png)

