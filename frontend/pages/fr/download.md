---
layout: page
lang: fr
permalink: /download/
title: Téléchargements
last_update: 2024-11-06
---

Notre dataset complet est disponible :


* <a download="pdh_data.csv" {% static_href %} href="/download/full.csv"{% endstatic_href %}>csv</a>, 
* <a download="pdh_data.parquet" {% static_href %} href="/download/full.parquet"{% endstatic_href %}>parquet</a>. 


## Exports
Les données peuvent aussi être exportées depuis la carte, ce qui permet de les filtrer. Attention, certaines données ne sont pas sur la carte: 
celles qui n'ont pas de coordonnées précises. Elles sont donc exclues de l'export via la carte.

L'export de la carte permet aussi de générer un fichier avec une colonne par substance (par défaut, toutes les valeurs 
des substances sont dans un champ de type json array, `pfas_values`). Attention, choisir cette option rajoute 
potentiellement des dizaines, voire des centaines de colonnes. 

Vous pouvez aussi exporter les données de chaque dataset depuis la page [datasets](/datasets).


## API
Nous avons développé une API minimaliste qui est utilisée par la carte pour afficher les données de chaque point. 
Libre à vous de l'utiliser aussi si c'est plus simple, vous trouverez une documentation (générée automatiquement) 
[ici](https://pdh.cnrs.fr/api/docs){:target="_blank"}. Attention, cette API peut-être modifiée à tout moment, si vous 
l'utilisez vraiment informez-nous pour qu'on en tienne compte. 

## Exemples d'analyses de données
Vous trouverez [ici](https://colab.research.google.com/drive/1D_U_HX_D3a9KNk-Y79lF0iehRynE6Rt9){:target="_blank"}
un notebook Google Collab avec différents exemples de travail sur les données (filtres, extractions, etc.)

<div class="alert alert-primary" role="alert">
N'hésitez pas à nous contacter (email en bas) si vous avez une question ou une difficulté dans l'analyse des données.
</div>
