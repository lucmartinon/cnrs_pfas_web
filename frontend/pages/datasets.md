---
layout: page
title: Datasets
permalink: /datasets/
---
<script type="text/javascript">

function filter_datasets(){
   filter = document.getElementById("dataset-filter").value.toLowerCase();
   const datasets = document.getElementsByClassName("accordion-item");
   for (let i = 0; i < datasets.length; i++) {
      dataset = datasets.item(i);
      dataset_name = dataset.getAttribute("data-name").toLowerCase();
      dataset_id = dataset.getAttribute("data-id").toLowerCase();

      if (dataset_name.includes(filter) || dataset_id.includes(filter)) {
         dataset.classList.remove("hidden");
      } else  {
         dataset.classList.add("hidden");
      }
   }
}


</script>


<div class="row">
    <label class="col-sm-2 col-form-label" for="dataset-filter">{{site.data[site.active_lang].strings['dataset_filter_label']}}</label>
    <div class="col-sm-10">
        <input type="text" id="dataset-filter"
          onchange   = "filter_datasets();"
          onkeypress = "this.onchange();"
          onpaste    = "this.onchange();"
          oninput    = "this.onchange();"
          autofocus style="width:100%;"/>
    </div>
</div>

<div class="accordion" id="accordion_datasets">
  {% for dataset in site.data.datasets %}
    {% if dataset.category == 'Sampling' %}
        {% assign class_cat = 'bg-info-subtle'  %}
    {% elsif dataset.category == "Known PFAS user" %}
        {% assign class_cat = 'bg-danger-subtle'  %}
    {% else %}
        {% assign class_cat = 'bg-success-subtle'  %}
    {% endif %}
     <div class="accordion-item" data-name="{{ dataset.dataset_name }}" data-id="{{ dataset.dataset_id }}">
       <div class="accordion-header" id="heading_{{ dataset.dataset_id }}">
         <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse_{{ dataset.dataset_id }}" aria-expanded="false" aria-controls="collapse_{{ dataset.dataset_id }}">
            <span class="badge badge-pill bg-primary-subtle text-dark me-3">{{ dataset.dataset_id }}</span>
            {{ dataset.dataset_name }}
            <span class="flex-grow-1 bd-highlight"> </span>
            {% assign cat_key = 'category|' | append: dataset.category %}
            <span class="badge badge-pill {{ class_cat }} text-black-50 me-3">{{ site.data[site.active_lang].strings[cat_key]  }}</span>&nbsp;
         </button>
       </div>
       <div id="collapse_{{ dataset.dataset_id }}" class="accordion-collapse collapse" aria-labelledby="heading_{{ dataset.dataset_id }}" data-bs-parent="#accordion_datasets">
         <div class="accordion-body">
            {% if dataset.dataset_comment and dataset.dataset_comment != '' %}
                <div class="row p-2  fw-lighter fs-6 fst-italic">{{dataset.dataset_comment | replace: "  
", " <BR/>"}}<BR/></div>
            {% endif %} 
            {% if dataset.blocking_problem and dataset.blocking_problem != '' %}
                <div class="callout callout-danger"><h4>{{ site.data[site.active_lang].strings['disabled_dataset'] }}</h4>{{dataset.blocking_problem}}</div>
            {% endif %} 
            <div class="row">
                {% assign src_type_key = 'source_type|' | append: dataset.source_type %}
                {% assign dcm_key = 'data_collection_method|' | append: dataset.data_collection_method %}
                <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|source"]}}</div>
                {% if dataset.source_url and dataset.source_url != '' %}
                    <div class="col-sm-9">
                        <a  href="{{dataset.source_url}}" target="_blank">{{dataset.source_text}}</a>
                        <span>({{ site.data[site.active_lang].strings[src_type_key] }}) {{ site.data[site.active_lang].strings[dcm_key] }}</span>
                    </div>
                {% else %}
                    <div class="col-sm-9">
                        {% if dataset.source_text %}{{ dataset.source_text }}{% else %}{{ site.data[site.active_lang].strings["diverse_sources"]}}{% endif %}
                        {% if dataset.source_type %}({{ site.data[site.active_lang].strings[src_type_key] }}) {{ site.data[site.active_lang].strings[dcm_key] }}{% endif %}
                    </div>
                {% endif %}
            </div>
            <div class="row">
                <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|first_added"]}}</div>
                <div class="col-sm-9">{{dataset.first_added | date: site.data[site.active_lang].i18n.date_format}} </div>
            </div>
            <div class="row">
                <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|last_processed"]}}</div>
                <div class="col-sm-9">{{dataset.last_processed | date: site.data[site.active_lang].i18n.date_format}}</div>
            </div>
            <div class="row">
                <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|type"]}}</div>
                <div class="col-sm-9">{{dataset.type}}</div>
            </div>
            <div class="row">
                <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|Rows"]}}</div>
                <div class="col-sm-9">{{dataset.Rows}}</div>
            </div>
            <div class="row">
                <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|Locations"]}}</div>
                <div class="col-sm-9">{{dataset.Locations}}</div>
            </div>
            {% if dataset.category == 'Sampling' %}
                <div class="row">
                    <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|PFAS"]}}</div>
                    <div class="col-sm-9">{{dataset['PFAS']}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|with values"]}}</div>
                    <div class="col-sm-9">{{dataset['with values']}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|above 10 ng/L"]}}</div>
                    <div class="col-sm-9">{{dataset['above 10 ng/L']}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|above 100 ng/L"]}}</div>
                    <div class="col-sm-9">{{dataset['above 100 ng/L']}}</div>
                </div>
            {% endif %}
                <div class="row">
                    <div class="col-sm-3 fw-light text-end">{{ site.data[site.active_lang].strings["dataset|links"]}}</div>
                    <div class="col-sm-9">                    
                        {% if dataset.raw_data_url %}
                            <a class="d-inline" target="_blank" href="{{dataset.raw_data_url}}">{{ site.data[site.active_lang].strings["dataset|raw_data"]}}</a>
                        {% endif %}
                        {% if dataset.extractor_url %}
                            <a class="d-inline" target="_blank" href="{{dataset.extractor_url}}">Extractor</a>
                        {% endif %}
                        <a class="d-inline" target="_blank" href="{{dataset.normaliser_url}}">Normaliser</a>
                    </div>
                </div>
            <div class="position-relative">
                <div class="btn-group position-absolute bottom-0 end-0 ">
                  <button type="button" class="btn btn-outline-primary dropdown-toggle {% if dataset.blocking_problem %} disabled {% endif %}" data-bs-toggle="dropdown" aria-expanded="false">
                    {{ site.data[site.active_lang].strings["datasets|download"] }}
                  </button>
                  <ul class="dropdown-menu">
                    <li><span class="dropdown-item" onclick="export_dataset({{dataset.dataset_id}}, 'csv');">CSV</span></li>
                    <li><span class="dropdown-item" onclick="export_dataset({{dataset.dataset_id}}, 'parquet');">parquet</span></li>
                  </ul>
                </div>
            </div>
         </div> 
       </div>
     </div>
  {% endfor %}
</div>

<script>
window.addEventListener("load", () => {
const headers = document.querySelectorAll(".accordion-header");
  headers.forEach(header => {
    header.addEventListener("click", function() {
      this.classList.toggle("active");
      const content = this.nextElementSibling;
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  });
});
});

function export_dataset(dataset_id, format){
    var params = {
        "dataset_id":dataset_id,
        "include_details":true,
        "file_format": format
    };

    var fn = "pdh_export." + (format == 'csv' ? 'csv.gz' : 'parquet');

    params = new URLSearchParams(params);
    var url = window.location.origin + '/api/export?' + params.toString();

    fetch(url)
        .then(response => response.blob())
        .then(blob => {
            var url = window.URL.createObjectURL(blob);
            var a = document.createElement('a');
            a.href = url;
            a.download = fn;
            document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
            a.click();
            a.remove();  //afterwards we remove the element again

            // we remove the spinner and re enable the button
            document.getElementById("export_button").removeAttribute("disabled");
            document.getElementById("export_spinner").classList.toggle ("collapse");

        });
}
</script>
