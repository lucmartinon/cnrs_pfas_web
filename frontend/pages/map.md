---
layout: map
title: Map
permalink: /map/
---
This is a geo-visualisation tool that we first created to check the validity of the data. We want to warn of potential caveats, that will be addressed in the future:

* A zone that shows no or few points only means that we gathered fewer data in this zone. It does not mean that the areas are less contaminated.
* Some points are not represented on the map because we only have a city and no precise coordinates. 
* Some coordinates are unprecise or even mistaken, especially in the dataset 1 (E-PRTR)
  
Samplings points: 
* The colour of the points is based on the concentration in Parts per Trillions (ng/l for liquids, ng/kg for solids). This means a 102 ng/l in surface water is represented similarly to a 102 ng/kg in biota or in sediment, although they don't necessarily represent a similar contamination. 
* For points with multiple samplings, the colour is based on the highest value for the sum of PFAS concentrations. A point can be red for a high value 10 years ago, even if since then the samplings have all been at 0. You can see all values in the details panel after clicking on the point. 
* For concentrations in solids, we have concentrations in fresh weight and in dry weight, and concentrations in weight without precision. They are for now shown with the same scale, which is not optimal.
* Some samplings points have all their measured concentrations below the limit of detection (LOD). These should not be considered as points with "no problem". First, early data can have very high LOD, so there could still be PFAS. Then, it's very important to look closely at which PFAS where measured. For example, a lot of points in France's drinking water dataset (124) have only values for a little number of uncommon PFAS. This means nothing for general PFAS contamination. 

If you are unsure how to interpret what you see, don't hesitate to contact us. 