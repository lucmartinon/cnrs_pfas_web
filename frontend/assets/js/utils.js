
var lang = 'en';
if(['/fr/', '/en/'].includes(window.location.pathname.substring(0, 4))){
    lang = window.location.pathname.substring(1, 3);
}

function encode_html(rawStr){
    return rawStr.replace(/[\u00A0-\u9999<>\&]/g, function(i) {
       return '&#'+i.charCodeAt(0)+';';
    });
}

function translate(key){
    if (translations){
        if (key in translations){
            if (lang in translations[key]){
                return translations[key][lang];
            } else {
                console.log("missing " + lang + " translation for key " + key);
                return translations[key]['en'];
            }
        } else {
            console.log("Missing key " + key);
        }
    } else {
        console.log("Translations not loaded yet: "+ key);
    }
    return key;
}

function format_date(date_str){
    if (date_str){
        return new Date(date_str).toLocaleDateString(lang == 'en' ? 'uk' : lang);
    } else {
        return null;
    }
}

async function load_stats(){
    const response = await fetch(window.location.origin + '/api/stats');
    const stats = await response.json();
    for (let [k, v] of Object.entries(stats)) {
        el = document.getElementById('stats_' + k);
        if (el != null){
            el.innerHTML = v.toLocaleString(lang);
        }
        else{
            console.log("EL not found: " + 'stats_' + k)
        }
    }
    for (var i = 0 ; i < stats.length ; i++){
    }

}