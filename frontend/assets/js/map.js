const map = L.map('map', {
    preferCanvas: true
});
map.setView([51.505, 3], 4);

const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

var data;
var map_data;
var datasets;
var filters_initialized = false;
var datasetFeatureGroup;
var info_div = document.getElementById('point_info');
var details_div = document.getElementById('point_details');
var colorScale = chroma.scale('OrRd').domain([0,100000]).classes([0,10,100,1000,10000,100000]);
var points_details;
var translations;


all_filters = [ 'category', 'dataset_id', 'matrix', 'cas_id'];
sampling_filters = ['matrix', 'cas_id'];

async function show_points_details(e){
    hide('dropdown_points_details');
    row = e.target.options.data;

    var filter_values = get_filter_values();
    filter_values.push(['lat', row.lat]);
    filter_values.push(['lon', row.lon]);

    params = new URLSearchParams(filter_values);

    const response = await fetch(window.location.origin + '/api/point_details?' + params.toString());
    res = await response.json();

    fields = [
        'name',
        'dataset_id',
        'dataset_name',
        'category',
        'sector',
        'matrix',
        'date',
        'year',
        'source_text',
        'source_type',
        'source_url',
        'pfas_sum',
        'unit',
        'pfas_values',
        'details'
    ];


    points_details = [];
    // first we convert them to objects, it's nicer to handle
    for (var i = 0 ; i < res.length ; i++){
        p = {};
        for (var j = 0 ; j < fields.length ; j++){
            p[fields[j]] = res[i][j];
        }
        points_details[i] = p;
    }

    if (points_details.length > 1){
        select = document.getElementById('dropdown_points_details');
        //first we empty the select
        while (select.options.length > 0) {
            select.remove(0);
        }
        for (var i = 0 ; i < points_details.length ; i++){
            var p = points_details[i];
            var unit = p.unit;
            var date = p.date == null ? p.year : format_date(p.date) ;
            var label = p.pfas_sum ? `${p.pfas_sum} ${unit}` : translate('map|legend|not_detected');
            select.options[select.options.length] = new Option(`${date} - ${label}`, i);
        }
        show('dropdown_points_details');
    }
    // we load the first result
    show_single_point_details(points_details[0])
}

function onChangeSelectPointDetail(event){
    var i = event.target.value;
    show_single_point_details(points_details[i])
}


function create_simple_row(label_html, content_html){
    var row = document.createElement('div');
    row.className = 'row';

    var label = document.createElement('div');
    label.className = 'col-sm-4 fw-light text-end';
    label.innerHTML = label_html;

    var value = document.createElement('div');
    value.className = 'col-sm-8';
    value.innerHTML = content_html;
    // if the value is a link, we put a link
    if (/^https?\:\/\//.test(content_html)){
        value.innerHTML = `<a target="_blank" href="${content_html}">${content_html}</a>`;
    }

    row.appendChild(label);
    row.appendChild(value);
    return row;
}

function create_pfas_tr(pv){
    var tr = document.createElement('tr');

    var td_name = document.createElement('td');
    td_name.className = 'w-50';
    td_name.innerHTML = pv.substance;


    var td_cas = document.createElement('td');
    td_cas.className = 'w-25 text-center';
    td_cas.innerHTML = `<a class="badge badge-pill bg-light text-dark fw-light text-decoration-none cas-badge" target="_blank" href="https://pubchem.ncbi.nlm.nih.gov/#query=${pv.cas_id}">${pv.cas_id}</a>`;

    var td_value = document.createElement('td');
    td_value.className = pv.value != null ? 'text-end w-25' : 'fw-light text-end w-25';
    td_value.innerHTML = pv.value != null ? `${pv.value} ${pv.unit}` : `< ${pv.less_than} ${pv.unit}`;

    tr.appendChild(td_name);
    tr.appendChild(td_cas);
    tr.appendChild(td_value);
    return tr;
}

function show_single_point_details(p){
    const container = document.getElementById('point_details');
    while (container.firstChild) {
        container.firstChild.remove()
    }
    var accordion =  document.getElementById("info_template").content.cloneNode(true);
    container.appendChild(accordion);

    // INFO
    var info = document.getElementById('content-info');

    var span = document.createElement('span');
    span.className = "badge badge-pill bg-light text-dark";
    span.innerHTML = p['dataset_id'];

    var row = create_simple_row(translate("Dataset"), span.outerHTML + '&nbsp;&nbsp;' + p['dataset_name']);
    info.appendChild(row);

    if (p.source_url) {
        var row = create_simple_row(translate("Source"), `<a target="_blank" href="${p.source_url}">${p.source_text}</a> (${translate("source_type|" + p.source_type)})`);
        info.appendChild(row);
    } else {
        var row = create_simple_row(translate("Source"), `${p.source_text} (${translate("source_type|" + p.source_type)})`);
        info.appendChild(row);
    }


    var simple_fields = ['name', 'category', 'sector', 'matrix', 'date'];
    var cat_fields = ['category', 'sector', 'matrix'];
    for (var i = 0 ; i < simple_fields.length ; i++){
        if (p[simple_fields[i]]){
            // we translate fields if they are in cat_fields
            var val = cat_fields.includes(simple_fields[i]) ? translate(simple_fields[i] + "|" + p[simple_fields[i]]) :  p[simple_fields[i]];

            // we format the date
            if (simple_fields[i] == 'date') {val = format_date(val);}

            // then we create the row
            var row = create_simple_row(translate(simple_fields[i]), val);
            info.appendChild(row);
        }
    }
    if (p['pfas_sum']){
        var row = create_simple_row(translate("pfas_sum"),  `${p.pfas_sum} ${p.unit}`);
        info.appendChild(row);
    }

    // PFAS CONCENTRATIONS
    if (p.pfas_values.length > 0){
        var concentrations = document.getElementById('content-concentrations');
        c_table = document.createElement('table');
        c_table.className = 'table table-borderless table-hover table-pfas-concentration';
        for (var i = 0 ; i < p.pfas_values.length ; i++){
            c_table.appendChild(create_pfas_tr(p.pfas_values[i]));
        }
        concentrations.appendChild(c_table);
    } else {
        document.getElementById('accordion-item-concentrations').remove();
    }

    // DETAILS
    if (p.details.key.length > 0){
        var details = document.getElementById('content-details');
        for (var i = 0 ; i < p.details.key.length ; i++) {
            details.appendChild(create_simple_row(p.details.key[i], p.details.value[i]));
        }
    } else {
        document.getElementById('accordion-item-details').remove();
    }
}

async function load_filter_options(filter_field){

    const filter_el = document.querySelector('#filter-' + filter_field);

    var filter_values = get_filter_values(filter_field)
    filter_values.push(['field', filter_field]);
    var params = new URLSearchParams(filter_values);

    const response = await fetch(window.location.origin + '/api/get_filter_options?' + params.toString());
    var res = await response.json();

    const options = [];
    res.forEach(function (row) {
        if (row.length == 2){
            options.push({
              value: row[0],
              label: row[1],
            });
        } else {
            options.push({
              value: row[0],
              label: translate(filter_field + '|' + row[0]),
            });
        }
    });
    filter_el.setOptions(options, true);
    if (filter_field == 'cas_id'){
        document.getElementById('substances_as_columns_warning').innerHTML = document.getElementById('substances_as_columns_warning').innerHTML.replace(/\(.*\)/, '(' + options.length +')')
    }
}

function get_filter_values(exclude){
    if (!filters_initialized){
        return [['category', 'Sampling']]
    }

    var params_array = []

    all_filters.forEach(function(f) {

        // if you want to update a filter options, you need to get the values of all other filters,
        // so you would call this function with the exclude param
        if (f != exclude){
            v = get_filter_value(f);
            if (v != null && v != "" && v != []){
                if (v.constructor === Array) {
                    for (s of v) {
                        params_array.push([f, s]);
                    }
                } else {
                    if (v != -1) {
                        params_array.push([f, v]);
                    }
                }
            }
        }
    });
    return params_array;
}

function legend_item(color, label){
    var legend_item = document.createElement('li');
    legend_item.className = "list-group-item row";
    var circle = document.createElement('img')
    circle.className = "border border-black rounded-circle p-0"
    circle.style = "background-color:" + color + ";height:10px;width:10px;"
    legend_item.appendChild(circle);
    var label_el  = document.createElement('span');
    label_el.className = "ml-5";
    label_el.innerHTML = label;
    legend_item.appendChild(label_el)
    return legend_item;
}

async function load_map_data(e){

    var startTime = performance.now()
    show('overlay');

    if (datasetFeatureGroup){
        datasetFeatureGroup.remove();
    }
    var filter_values = get_filter_values();
    var params = new URLSearchParams(filter_values);
    const response = await fetch(window.location.origin + '/api/map_data?' + params.toString());
    var answerTime = performance.now()
    res = await response.json();
    map_data = [];
    fields = ['lat', 'lon', 'name', 'category', 'type', 'pfas_sum', 'records'];
    var records = 0;
    // first we convert them to objects, it's nicer to handle
    for (var i = 0 ; i < res.length ; i++){
        p = {};
        for (var j = 0 ; j < fields.length ; j++){
            p[fields[j]] = res[i][j];
        }
        map_data[i] = p;
        records += p['records'];
    }


    var jsonTime = performance.now()

    datasetFeatureGroup = L.featureGroup();
    map_data.forEach(function (row) {
        var circle = L.circleMarker([row.lat, row.lon], {
            radius: 5,
            color:'black',
            weight:.5,
            opacity:1,
            fillColor: getColor(row),
            fillOpacity:1,
            data:row
        }).bindPopup(row.name);
        circle.addTo(datasetFeatureGroup);
        circle.on("click", show_points_details);
    });
    datasetFeatureGroup.addTo(map);


    var mapTime = performance.now();
    console.log(`Total load time: ${mapTime - startTime} milliseconds, for ${map_data.length} lines`);
    console.log(`\t server answer ${answerTime - startTime} `);
    console.log(`\t (server work time: ${response.headers.get("x-process-time")})`);
    console.log(`\t rebuilding json ${jsonTime - answerTime} `);
    console.log(`\t adding to the map ${mapTime - jsonTime} `);
    hide('overlay');
    document.getElementById('record_count').textContent="(" + records + " " + translate("records") + ")";

    //legend
    var legend_el = document.getElementById("legend_ul");
    var selected_cat = get_filter_value("category");
    if (selected_cat == 'Sampling'){
        legend_el.innerHTML = translate('map|legend_header_samplings');
        const vals = [null,1,10,100,1000,10000];
        const labels = [encode_html(translate('map|legend|not_detected')) ,'<10 ppt', '10 - 100 ppt', '100 - 1,000 ppt','1,000 - 10,000 ppt','> 10,000 ppt'];
        vals.map(function(v, i) {
            var color = getColor({'category':'Sampling', 'pfas_sum': v})
            legend_el.appendChild(legend_item(color, labels[i]))
        });
    }else {
        legend_el.innerHTML = "";
        for (const [label, color] of Object.entries(COLORS_PRESUMPTIVE)) {
            legend_el.appendChild(legend_item(color, label + "   "));
        }

    }


}

d3.csv('../assets/data/translations.csv').then(function(d) {
    translations = {};
    for (const row of d){
        translations[row.key] = {};
        for (const l of ['en', 'fr']){
            translations[row.key][l] = row[l];
        }
    }
    console.log('loaded translations');
    init_filters();
});

const COLORS_PRESUMPTIVE = {
    'Industrial site': rgbToHex(68, 92, 151),
    'Waste management site': rgbToHex(120, 80, 19),
    'Airport': rgbToHex(74, 165, 216),
    'Military site': rgbToHex(153, 168, 94),
    'Sampling location': rgbToHex(134, 68, 151),
    'Firefighting incident / training': rgbToHex(238, 173, 7),
    'PFAS production facility': rgbToHex(134, 68, 151),
    'Other': rgbToHex(154, 156, 164),
};


function getColor(row){
    if (row.category == 'Sampling'){
        if (!row.pfas_sum){
            return '#E0E0E0';
        }
        return colorScale(row.pfas_sum).hex();
    }
    else if (row.category == 'Known PFAS user'){
        return 'orange';
    }
    else if (row.category == 'Presumptive'){
        return COLORS_PRESUMPTIVE[row.type];
    }
}

function hide(div_id){
    document.getElementById(div_id).classList.add('hidden');
    document.getElementById(div_id).classList.remove('visible');
}

function show(div_id) {
    document.getElementById(div_id).classList.add('visible');
    document.getElementById(div_id).classList.remove('hidden');
}

function get_filter_value(filter){
    f = document.querySelector('#filter-' + filter)
    return f.value;
}

async function init_filters(){

    VirtualSelect.init({
      ele: '#filter-category',
      options: [
        { label: translate("category|Sampling"),         value: 'Sampling' },
        { label: translate("category|Presumptive"),      value: 'Presumptive' },
        { label: translate("category|Known PFAS user"),  value: 'Known PFAS user' },
        { label: translate("category|PFAS production facility"),  value: 'PFAS production facility' },
      ],
      zIndex: 1500,
      selectedValue: "Sampling",
      hideClearButton: true,
    });

    VirtualSelect.init({
      ele: '#filter-dataset_id',
      placeholder: 'Dataset',
      zIndex: 1500,
      multiple: true,
      search: true
    });


    VirtualSelect.init({
      ele: '#filter-matrix',
      placeholder: translate("matrix"),
      zIndex: 1500,
      multiple: true,
      search: true,
      showValueAsTags: true,
    });


    VirtualSelect.init({
      ele: '#filter-cas_id',
      placeholder: translate("Substance"),
      zIndex: 1500,
      multiple: true,
      search: true
    });

    console.log("Filters initialized, adding onChange events.")

    await new Promise(r => setTimeout(r, 300));
    all_filters.forEach(function(f){
        if (f != 'category') {
            //This also sets the onchange
            load_filter_options(f);
        }
        document.querySelector('#filter-' + f).addEventListener('change', onChangeFilters);

    });
    filters_initialized = true;
}


function onChangeFilters(e){
    console.log('value from e: ' + e.value);
    var el = e.target;

    // if we typed in the search field, the element that triggers the onchange is an internal el from the filter.
    // in this case we follow the hierarchy upwards until finding the filter.
    while(el.id.slice(0,6) != 'filter'){
        el = el.parentElement;
    }

    if (e.target.id.slice(0,6) == 'vscomp') {
        // The event is fired from the textfield and not from the filter, this is a bug

        var current_values = el.value;

        var target_el = e.explicitOriginalTarget
        while (typeof target_el.dataset.value == 'undefined'){
            target_el = target_el.parentElement;
        }
        var value_just_clicked = target_el.dataset.value;

        // console.log('value_just_clicked: ' + value_just_clicked);
        var idx = current_values.indexOf(value_just_clicked)
        if (idx >= 0){
            current_values.splice(idx, 1);
        }else{
            current_values.push(value_just_clicked);
        }
        el.setValue(current_values, true);

    }

    if (filters_initialized){
        changed_filter =  el.id.slice(7);
        var cat = get_filter_value("category");

        // we check each filter
        all_filters.forEach(function(f){
            // if category was changed, we reset other filters and show / hide sampling filters
            if (changed_filter == 'category'){
                if (sampling_filters.includes(f)){
                    if (cat == 'Sampling') {
                        show('filter-' + f);
                    }else {
                        hide('filter-' + f);
                    }
                }
                // if we change cat we reset the 3 other filters
                if (f != 'category'){
                    document.querySelector('#filter-' + f).reset(true, true);
                }
            }

            // In any case we reload all filters except category and the changed filter
            if (f != 'category' && f != changed_filter){
                console.log('reload ' + f);
                load_filter_options(f);
            }
        });
        load_map_data();
    }
}

async function generate_export(){
    document.getElementById("export_button").setAttribute("disabled", true);
    document.getElementById("export_spinner").classList.toggle ("collapse");
    var format = "parquet";
    if (document.getElementById('export_csv').checked){
        format = "csv";
    }


    var filters_values = get_filter_values();
    filters_values.push(['file_format', format]);
    filters_values.push(['include_details', document.getElementById('include_details').checked]);
    filters_values.push(['substances_as_columns', document.getElementById('substances_as_columns').checked]);

    var fn = "pdh_export." + (format == 'csv' ? 'csv.gz' : 'parquet');

    params = new URLSearchParams(filters_values);
    var url = window.location.origin + '/api/export?' + params.toString();

    fetch(url)
        .then(response => response.blob())
        .then(blob => {
            var url = window.URL.createObjectURL(blob);
            var a = document.createElement('a');
            a.href = url;
            a.download = fn;
            document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
            a.click();
            a.remove();  //afterwards we remove the element again

            // we remove the spinner and re enable the button
            document.getElementById("export_button").removeAttribute("disabled");
            document.getElementById("export_spinner").classList.toggle ("collapse");

        });
}

function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}




load_map_data();