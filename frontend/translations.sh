#!/usr/bin/env bash
INPUT=assets/data/translations.csv
OLDIFS=$IFS
IFS=','
skip_headers=1
while read key en fr
do
    if ((skip_headers))
    then
        # First line only: we empty the yml files
        ((skip_headers--))
        > _data/en/strings.yml
        > _data/fr/strings.yml
    else
      echo "${key}: ${en}" >> _data/en/strings.yml
      echo "${key}: ${fr}" >> _data/fr/strings.yml
    fi
done < $INPUT
IFS=$OLDIFS
echo "Generated translations string files from translations.csv"