---
layout: page
lang: en
permalink: /
---

![logo](../assets/images/pdh_logo_with_name.png "Logo of the PFAS Data Hub project")


We currently have  <span id="stats_count"/> rows in the database:
* <span id="stats_count_presumptive"/> of category "Presumptive",
* <span id="stats_count_known_users"/> of category "Known PFAS user",
* <span id="stats_count_sampling"/> of category "Sampling", including <span id="stats_count_sampling_with_loc"/> with precise location (some other points only have the city but no coordinates, these are not shown on the map)

This data is coming from <span id="stats_count_datasets"/> different datasets (see [here](/datasets)), 
including <span id="stats_count_datasets_science"/> scientific articles 
and <span id="stats_count_datasets_authorities"/> datasets coming from authorities. 

<script>
load_stats();
</script>