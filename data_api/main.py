import os
from datetime import datetime
from typing import Annotated
import time
import logging
import time

from fastapi import FastAPI, Request, Depends, Query, HTTPException
from starlette.responses import FileResponse
from duckdb import ParserException, InvalidInputException
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from fastapi_cache.decorator import cache
from redis import asyncio as aioredis
from app import duck

INITIALISED = False

# init
app = FastAPI(root_path="/api")
duck.init()

# we remove files from the /tmp folder. This will run once a day at least when deploy_prod runs.
for f in os.listdir('/tmp'):
    if f.startswith('pdh_export'):
        os.unlink(f'/tmp/{f}')


@cache()
async def get_cache():
    return 1


@app.on_event("startup")
async def startup():
    global INITIALISED
    redis = aioredis.from_url("redis://redis")
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")

    logging.info("Redis initialized")
    time.sleep(10)
    INITIALISED = True


def common_filters(
        dataset_id: Annotated[list[int], Query()] = None,
        category: Annotated[list[str], Query()] = None,
        matrix: Annotated[list[str], Query()] = None,
        cas_id: Annotated[list[str], Query()] = None
):
    return {
        "dataset_id": dataset_id,
        "category": category,
        "matrix": matrix,
        "cas_id": cas_id
    }


@app.get("/map_data")
@cache(expire=80000)
def map_data(filters: Annotated[dict, Depends(common_filters)]):
    """
    Returns the data necessary to show the points on the map, filtered.
    Even if many lines are on this spot, only one line is returned
    Parameters:
    - `dataset_id`: filters on dataset_id
    - `category`: filters on a category (Sampling, Presumputive, Known PFAS users, PFAS production facility)
    - `matrix`: filters on the matrix (Groundwater, Surface water, Biota, etc.)
    - `cas_id`: filters on the substances

    Returns: an array of points on the map, also as an array. Fields of the array:
    lat, lon, name, category, type, pfas_sum (highest sum), records (= count of records at these coordinates)
    """
    query = """
            select distinct
                lat, lon, name, category, type,
                case 
                    when $cas_id is null then max(pfas_sum) 
                    else max(list_aggregate([pv.value::float for pv in pfas_values if pv.value not in ('null', 'NaN') and list_contains($cas_id, pv.cas_id)], 'sum')) 
                end pfas_sum, 
                count(*) records
            from data 
            where 
                lat is not null and lon is not null and
                ($dataset_id is null or dataset_id = any ($dataset_id)) and 
                ($category is null or category = any ($category)) and 
                ($matrix is null or matrix = any ($matrix)) and 
                ($cas_id is null or [pv.cas_id for pv in pfas_values] && $cas_id) 
            group by all
        """
    return execute_query(query, filters)


@app.get("/export")
def export(
        params: Annotated[dict, Depends(common_filters)],
        file_format: str = 'csv',
        include_details: bool = False,
        substances_as_columns: bool = False
) -> FileResponse:
    """
    Exports the data to a file, then returns it. Parameters:
    - `file_format`: csv or parquet
    - `include_details`: if true, the details col will be included
    - `substances_as_columns`: if true, we pivot the result on the substance name, leading to a lot of columns, but no json field
    - `dataset_id`: filters on dataset_id
    - `category`: filters on a category (Sampling, Presumputive, Known PFAS users, PFAS production facility)
    - `matrix`: filters on the matrix (Groundwater, Surface water, Biota, etc.)
    - `cas_id`: filters on the substances
    

    Returns the exported file
    """
    ending = f'csv.gz' if file_format == 'csv' else 'parquet'
    fn = f'pdh_export_{datetime.now().strftime("%Y-%m-%d_%H%M%S")}.{ending}'
    fp = f'/tmp/{fn}'

    # we first make a filtered table, because otherwise we cannot pivot it
    exclude_details = '' if include_details else 'EXCLUDE (details)'

    replace_details = '' if not include_details else 'details::json as details, '

    replace = f'''REPLACE (
        {replace_details}
        case 
            when $cas_id is null then pfas_values
            else [pv for pv in pfas_values if list_contains($cas_id, pv.cas_id)]
        end as pfas_values )'''

    where = """where
    ($dataset_id is null or dataset_id = any ($dataset_id)) and 
    ($category is null or category = any ($category)) and 
    ($matrix is null or matrix = any ($matrix)) and 
    ($cas_id is null or [pv.cas_id for pv in pfas_values] && $cas_id) 
    """
    prep_query = f'CREATE OR REPLACE TABLE filtered_data as (select * {exclude_details} {replace} from  data {where})'
    execute_query(prep_query, params, False)

    # then we query it or pivot it
    if substances_as_columns:
        query = f"""
pivot (
    select 
        * exclude (pvs), 
        pvs.cas_id || ' ('|| left(pvs.substance, 60) ||')' pfas, 
        case when pvs.value is null then '<' || pvs.less_than else pvs.value::VARCHAR end as str_value 
    from (
        select * exclude (pfas_values), unnest(pfas_values) pvs 
        from filtered_data
    )
) on pfas using max(str_value)    
"""
    else:
        query = f"""
select 
    COLUMNS(* REPLACE (pfas_values::json AS pfas_values) )
from filtered_data
        """

    execute_query(f"copy({query}) to '{fp}'")
    logging.info(f'generated file {fp}')

    return FileResponse(path=fp, filename=fn, media_type="multipart/form-data")


@app.get('/point_details')
@cache(expire=80000)
def point_details(lat: float, lon: float, filters: Annotated[dict, Depends(common_filters)]):
    """
    Returns the detailed data of a point shown on the map.
    Returns many lines if there are multiple lines for these coordinates
    (happens often when there are many sampling over time)
    Parameters:
    - `dataset_id`: filters on dataset_id
    - `category`: filters on a category (Sampling, Presumputive, Known PFAS users, PFAS production facility)
    - `matrix`: filters on the matrix (Groundwater, Surface water, Biota, etc.)
    - `cas_id`: filters on the substances

    Returns: an array detailed data, also as an array. Fields of the array:
    - name,
    - dataset_id,
    - dataset_name,
    - category,
    - sector,
    - matrix,
    - date,
    - year,
    - source_text,
    - source_type,
    - source_url,
    - pfas_sum,
    - unit,
    - pfas_values,
    - details
    """
    filters['lat'] = lat
    filters['lon'] = lon
    query = """
            select 
                name,
                dataset_id, 
                dataset_name,
                category, 
                sector, 
                matrix,
                date, 
                year,
                source_text, 
                source_type,
                source_url, 
                case 
                    when $cas_id is null then pfas_sum
                    else list_aggregate([pv.value::float for pv in pfas_values if pv.value not in ('null', 'NaN') and list_contains($cas_id, pv.cas_id)], 'sum')
                end pfas_sum, 
                unit,
                case 
                    when $cas_id is null then pfas_values
                    else [pv for pv in pfas_values if list_contains($cas_id, pv.cas_id)]
                end pfas_values, 
                details
            from data 
            where 
                lat = $lat and lon = $lon and
                ($category is null or category = any ($category)) and 
                ($matrix is null or matrix = any ($matrix)) and 
                ($dataset_id is null or dataset_id = any ($dataset_id)) and
                ($cas_id is null or [pv.cas_id for pv in pfas_values] && $cas_id) 
            order by date desc
        """
    return execute_query(query, filters)


@app.get("/count")
@cache(expire=80000)
def count():
    with duck.get_con() as con:
        return {"duck_count": con.sql("select count(*) from data").fetchone()[0]}


@app.get("/stats")
@cache(expire=80000)
def stats():
    res = {}
    with duck.get_con() as con:
        res['count'] = con.sql("select count(*) from data").fetchone()[0]
        res['count_presumptive'] = con.sql("select count(*) from data where category = 'Presumptive'").fetchone()[0]
        res['count_Sampling'] = con.sql("select count(*) from data where category = 'Sampling'").fetchone()[0]
        res['count_Sampling_with_loc'] = con.sql(
            "select count(*) from data where category = 'Sampling' and lat is not null and lon is not null").fetchone()[0]
        res['count_known_users'] = con.sql("select count(*) from data where category = 'Known PFAS user'").fetchone()[0]
        res['count_datasets'] = con.sql("select count(distinct dataset_id) from data").fetchone()[0]
        res['count_datasets_science'] = \
            con.sql("select count(distinct dataset_id) from data where source_type = 'Scientific article'").fetchone()[
                0]
        res['count_datasets_authorities'] = \
            con.sql("select count(distinct dataset_id) from data where source_type = 'Authorities'").fetchone()[0]

    return res


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    logging.info(f'Request {request.url} answered in {1000 * process_time} ms')
    return response


@app.get("/get_filter_options")
@cache(expire=80000)
def get_filter_options(
        field: Annotated[str, Query()] = None,
        filters: Annotated[dict, Depends(common_filters)] = None):
    """
    Returns the list of possible filters values for the given filters.
    - `field`: the values you want to see.

    - `dataset_id`: filters on dataset_id
    - `category`: filters on a category (Sampling, Presumputive, Known PFAS users, PFAS production facility)
    - `matrix`: filters on the matrix (Groundwater, Surface water, Biota, etc.)
    - `cas_id`: filters on the substances
    """
    if field in filters:
        del filters[field]

    if field == 'dataset_id':
        query = """
            select distinct dataset_id, '[' || dataset_id || '] ' || dataset_name as dataset_name
            from data
            where lat is not null and lon is not null and
                ($category is null or category = any ($category)) and 
                ($matrix is null or matrix = any ($matrix)) and 
                ($cas_id is null or [pv.cas_id for pv in pfas_values] && $cas_id) 
            order by 1
            """
    if field == 'matrix':
        query = """
            select distinct matrix
            from data
            where lat is not null and lon is not null and matrix is not null and 
                ($category is null or category = any ($category)) and 
                ($dataset_id is null or dataset_id = any ($dataset_id)) and
                ($cas_id is null or [pv.cas_id for pv in pfas_values] && $cas_id) 
            order by 1
            """

    elif field == 'cas_id':
        query = """
        select cas_id, split_part(substance, '_', 1) substance from (
            select  
            unnest(pfas_values).cas_id as cas_id,
            unnest(pfas_values).substance as substance
            from data
                where lat is not null and lon is not null and
                ($matrix is null or matrix = any ($matrix)) and 
                ($category is null or category = any ($category) ) and 
                ($dataset_id is null or dataset_id = any ($dataset_id)) 
        )
        group by ALL 
        order by count(*) desc
        """

    with duck.get_con() as con:
        return con.execute(query, filters).fetchall()


@app.get("/health")
def health_check():
    if not INITIALISED:
        raise HTTPException(status_code=503, detail="Server starting")
    return {"status": "healthy"}


def execute_query(query, params=None, read_only=True):
    try:
        with duck.get_con(read_only) as con:
            return con.execute(query, params).fetchall()
    except (ParserException, InvalidInputException) as e:
        logging.error(f"DuckDB returned the following error \n{e}\n in query \n {query}\n\n Params:\n{params}")
        raise e
