import duckdb
import logging

from app.settings import DATA_PARQUET_FP

DUCK_FP = 'duck.duckdb'


def init():
    with get_con(read_only=False) as con:
        con.sql(f"""
        create or replace table data as (
            select
              COLUMNS(* REPLACE (pfas_values::struct(
                cas_id VARCHAR, 
                unit VARCHAR, 
                substance VARCHAR, 
                isomer VARCHAR, 
                less_than VARCHAR, 
                value VARCHAR)[] AS pfas_values, 
                details::json::map(VARCHAR, VARCHAR) AS details, year::int AS year, date::date AS date))
            from '{DATA_PARQUET_FP}'
        );""")
    logging.info("Loaded table in memory duckdb instance")


def get_con(read_only=True):
    return duckdb.connect(DUCK_FP, read_only=read_only)