from dotenv import load_dotenv
import os
load_dotenv()

DATA_FOLDER = '/data'
DATA_PARQUET_FP = os.path.join(DATA_FOLDER, 'full.parquet')